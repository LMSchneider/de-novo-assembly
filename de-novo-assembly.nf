nextflow.enable.dsl = 2

params.kmerlength = 71
params.cov_cutoff = 0
params.min_contig_lgth = 2

params.with_velvet = false
params.with_spades = false


process velvet {
  publishDir "${params.outdir}", mode : "copy", overwrite : true
  container "https://depot.galaxyproject.org/singularity/velvet%3A1.2.10--h5bf99c6_4 "
  input:
    path fastqfiles
  output:
    path "*", emit: velvetfiles
    path "velvet_contigs_${fastqfiles.getSimpleName()}.fasta", emit: velvetcontigs
  script:
    """
    velveth out/ ${params.kmerlength} -fastq -short ${fastqfiles} > results
    velvetg out/ -cov_cutoff ${params.cov_cutoff} -min_contig_lgth ${params.min_contig_lgth}
    cp out/*.fa  velvet_contigs_${fastqfiles.getSimpleName()}.fasta
    """
}



process spades {
  publishDir "${params.outdir}", mode : "copy", overwrite : true
  container "https://depot.galaxyproject.org/singularity/spades%3A3.15.3--h95f258a_0"
  input:
    path fastqfiles
  output:
    path "*", emit: spadesfiles
    path "spades_contigs_${fastqfiles.getSimpleName()}.fasta", emit: spadescontigs
  script:
    """
    spades.py -s ${fastqfiles} -o spades
    mv spades/contigs.fasta spades_contigs_${fastqfiles.getSimpleName()}.fasta 
    """
}

process quast {
  publishDir "${params.outdir}", mode : "copy", overwrite: true
  container "https://depot.galaxyproject.org/singularity/quast%3A5.0.2--py36pl5262h30a8e3e_4"
  input:
    path contigfiles
  output:
    path  "*"
  script:
    """
    python /usr/local/bin/quast ${contigfiles}
    """
}

workflow assembly {
  take:
    fastqfilechannel
    outdir
    kmerlength
    cov_cutoff
    min_contig_lgth
    with_velvet
    with_spades

  main:
    params.assembly_outdir = outdir
      if (with_velvet) {
        velvetchannel = velvet(fastqfilechannel)
      }
      if (with_spades) {
    spadeschannel = spades(fastqfilechannel.flatten())
      }
    schannel = spadeschannel.spadescontigs
    vchannel = velvetchannel.velvetcontigs
    contigfiles = schannel.concat(vchannel).collect()
    contigfiles.view()
     result = quast(contigfiles)
  emit:
    result
}



workflow {
  fastqfilechannel = channel.fromPath("${params.indir}/*fastq").collect()
  
  assembly(fastqfilechannel, params.outdir, params.kmerlength, params.cov_cutoff,params.min_contig_lgth, params.with_velvet, params.with_spades)
}







