nextflow.enable.dsl = 2


include { vorbereitung } from "/home/lisa/ngsmodule/ngs-nextflow-pipeline/prefetch_singularity_uebung"
include { assembly } from "/home/lisa/ngsmodule/de-novo-assembly/de-novo-assembly"
include { multiqc } from "/home/lisa/ngsmodule/ngs-nextflow-pipeline/prefetch_singularity_uebung"


params.with_fastqc = false
params.with_fastp = false
params.with_stats = false
params.with_velvet = false
params.with_spades = false
params.multiqc = false
params.kmerlength = 91
params.cov_cutoff = 20
params.min_contig_lgth = 200
params.quast = null




workflow {
    vorbereitungresult = vorbereitung(params.accession, params.outdir, params.with_stats, params.with_fastqc, params.with_fastp)
    assemblyresult = assembly(vorbereitungresult.results_trimmed, params.outdir, params.kmerlength, params.cov_cutoff,params.min_contig_lgth, params.with_velvet, params.with_spades)
    multiqc_input = vorbereitungresult.fastqc_results
            .concat(vorbereitungresult.fastas_trimmed_json)
            .concat(assemblyresult.result).collect()
    multiqc(multiqc_input)
}

